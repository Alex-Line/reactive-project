package com.example.reactiveproject.domain;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Builder
public record Review(BigDecimal rating, Long reviewsCount) {}