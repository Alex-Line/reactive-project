package com.example.reactiveproject.service;

import java.io.IOException;

public interface PageProducer {

    String getPage(String s);

}