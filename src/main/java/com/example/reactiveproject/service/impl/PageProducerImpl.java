package com.example.reactiveproject.service.impl;

import com.example.reactiveproject.service.PageProducer;
import jakarta.annotation.PostConstruct;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class PageProducerImpl implements PageProducer {

    @PostConstruct
    public void init(){
        this.getPage("https://www.trustpilot.com/review/gullwingmotor.com");
    }

    @Override
    public String getPage(String path) {
        try {
            Document doc = Jsoup.connect(path).get();
            // TODO
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}