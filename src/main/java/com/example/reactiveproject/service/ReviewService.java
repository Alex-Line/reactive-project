package com.example.reactiveproject.service;

import com.example.reactiveproject.domain.Review;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class ReviewService {

    private String basePath = "https://www.trustpilot.com/review/";
    private final PageProducer producer;

    public Mono<Review> getReview(final String company) {
        var page = producer.getPage(basePath + company);
        return null;
    }

}