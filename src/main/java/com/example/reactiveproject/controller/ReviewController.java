package com.example.reactiveproject.controller;

import com.example.reactiveproject.domain.Review;
import com.example.reactiveproject.service.ReviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("/review")
public class ReviewController {

    private final ReviewService service;

    @GetMapping("/{company}")
    public Mono<Review> getReviews(@PathVariable String company) {
        return service.getReview(company);
    }

}